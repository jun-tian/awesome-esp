# Contribution Guidelines贡献指南

Please note that this project is released with a请注意，此项目发布时带有
[Contributor Code of Conduct](code-of-conduct.md). By participating in this
project you agree to abide by its terms.通过参与这个您同意遵守其条款的项目。

---

Ensure your pull request adheres to the following guidelines:

确保您的拉取请求符合以下准则：

- Make sure you contribution is really ESP8266/ESP32 related. It doesn't have to be a hardware project, but it can't be just a general purpose microcontroller tool, by example.

确保您的贡献确实与 ESP8266/ESP32 相关。它不一定是一个硬件项目，但它不能只是一个通用的微控制器工具，例如。

- Make sure the links are working and if possible, HTTPS Enabled (you can use the Travis build).

确保链接正常工作，如果可能，启用 HTTPS（您可以使用 Travis 构建）。

- Avoid redirects if possible.

尽可能避免重定向。

Thank you for your suggestions!谢谢你的建议！


## Updating your PR更新你的 PR

A lot of times, making a PR adhere to the standards above can be difficult.
If the maintainers notice anything that we'd like changed, we'll ask you to
edit your PR before we merge it. There's no need to open a new PR, just edit
the existing one. If you're not sure how to do that,

很多时候，让 PR 遵守上述标准可能很困难。如果维护人员注意到我们想要更改的任何内容，
我们会要求您在我们合并之前编辑您的 PR。无需打开新的 PR，只需编辑现有的。
如果你不确定该怎么做

[here is a guide](https://github.com/RichardLitt/knowledge/blob/master/github/amending-a-commit-guide.md)
on the different ways you can update your PR so that we can merge it.
您可以通过不同的方式更新您的 PR，以便我们可以合并它。
